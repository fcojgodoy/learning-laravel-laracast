<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::resource('projects', 'ProjectsController', [
    'except' => ['edit', 'create']
]);

// php artisan route:list
// +--------+-----------+------------------------+------------------+-------------------------------------------------+--------------+
// | Domain | Method    | URI                    | Name             | Action                                          | Middleware   |
// +--------+-----------+------------------------+------------------+-------------------------------------------------+--------------+
// |        | GET|HEAD  | /                      |                  | Closure                                         | web          |
// |        | GET|HEAD  | api/projects           | projects.index   | App\Http\Controllers\ProjectsController@index   | api          |
// |        | POST      | api/projects           | projects.store   | App\Http\Controllers\ProjectsController@store   | api          |
// |        | GET|HEAD  | api/projects/{project} | projects.show    | App\Http\Controllers\ProjectsController@show    | api          |
// |        | PUT|PATCH | api/projects/{project} | projects.update  | App\Http\Controllers\ProjectsController@update  | api          |
// |        | DELETE    | api/projects/{project} | projects.destroy | App\Http\Controllers\ProjectsController@destroy | api          |
// |        | GET|HEAD  | api/user               |                  | Closure                                         | api,auth:api |
// +--------+-----------+------------------------+------------------+-------------------------------------------------+--------------+
